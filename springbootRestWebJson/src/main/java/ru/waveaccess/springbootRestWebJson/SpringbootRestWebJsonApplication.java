package ru.waveaccess.springbootRestWebJson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRestWebJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRestWebJsonApplication.class, args);
	}
}
