package ru.waveaccess.springbootRestWebJson.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.waveaccess.springbootRestWebJson.domain.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

}
