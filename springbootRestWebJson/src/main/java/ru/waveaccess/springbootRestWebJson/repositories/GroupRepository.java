package ru.waveaccess.springbootRestWebJson.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.waveaccess.springbootRestWebJson.domain.Group;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

}