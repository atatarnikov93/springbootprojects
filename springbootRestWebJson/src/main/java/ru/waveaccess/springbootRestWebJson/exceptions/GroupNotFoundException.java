package ru.waveaccess.springbootRestWebJson.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "entry not found")
public class GroupNotFoundException extends RuntimeException {

	public GroupNotFoundException(String exception) {
		super(exception);
	}

}
