package ru.waveaccess.springbootRestWebJson.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.*;
import ru.waveaccess.springbootRestWebJson.domain.Group;
import ru.waveaccess.springbootRestWebJson.domain.Student;
import ru.waveaccess.springbootRestWebJson.exceptions.GroupNotFoundException;
import ru.waveaccess.springbootRestWebJson.exceptions.StudentNotFoundException;
import ru.waveaccess.springbootRestWebJson.services.GroupService;
import ru.waveaccess.springbootRestWebJson.services.StudentService;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.ArrayList;

@RestController
@RequestMapping("rest/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final GroupService groupService;

//    @GetMapping
//    public Iterable<Student> getListOfStudents() {
//        return studentService.findAll();
//    }

    @GetMapping
    public Iterable<StudentDTO> getListOfStudents() {
        Iterable<Student> allStudents = studentService.findAll();
        Iterable<StudentDTO> allSDTO = new ArrayList<>();
        allStudents.forEach(student -> {
            Long groupId = student.getGroup().getId();
            StudentDTO studentDTO = new StudentDTO();
            studentDTO.id = student.getId();
            studentDTO.birthday = student.getBirthday();
            studentDTO.firstName = student.getFirstName();
            studentDTO.groupId = groupId;
            studentDTO.lastName = student.getLastName();
            studentDTO.middleName = student.getMiddleName();
            ((ArrayList<StudentDTO>) allSDTO).add(studentDTO);
        });
        return allSDTO;
    }

    @DeleteMapping("{id}")
    public void deleteStudent(@PathVariable Long id) {
        // Поиск студента, если нет, то бросаем исключение. Если да, то пойдем дальше построчно по коду
        Student student = studentService.findStudent(id).orElseThrow(() -> new StudentNotFoundException("Student not found"));
        studentService.deleteStudent(student);
    }

    @PostMapping
    public void createStudent(@RequestBody StudentDTO studentDTO) {
        Student newStudent = new Student();
        Long groupId = studentDTO.getGroupId();
        Group group = groupService.findGroup(groupId).orElseThrow(() -> new GroupNotFoundException("Group not found"));
        newStudent.setGroup(group);
        newStudent.setBirthday(studentDTO.getBirthday());
        newStudent.setFirstName(studentDTO.getFirstName());
        newStudent.setMiddleName(studentDTO.getMiddleName());
        newStudent.setLastName(studentDTO.getLastName());
        studentService.createStudent(newStudent);
    }

    @PutMapping("{id}")
    public void updateStudent(@RequestBody StudentDTO studentDTO) {
        Long id = studentDTO.getId();
        Student newStudent = studentService.findStudent(id).orElseThrow(() -> new StudentNotFoundException("Student not found"));
        Long groupId = studentDTO.getGroupId();
        Group group = groupService.findGroup(groupId).orElseThrow(() -> new GroupNotFoundException("Group not found"));
        newStudent.setGroup(group);
        newStudent.setBirthday(studentDTO.getBirthday());
        newStudent.setFirstName(studentDTO.getFirstName());
        newStudent.setMiddleName(studentDTO.getMiddleName());
        newStudent.setLastName(studentDTO.getLastName());
        studentService.updateStudent(newStudent);
    }


    private static class StudentDTO {
        @Getter
        private Long id;

        @NotBlank
        @Getter
        @Setter
        private String firstName;

        @NotBlank
        @Getter
        @Setter
        private String middleName;

        @NotBlank
        @Getter
        @Setter
        private String lastName;

        @NotBlank
        @Getter
        @Setter
        private LocalDate birthday;

        @NotBlank
        @Getter
        @Setter
        private Long groupId;
    }

}
