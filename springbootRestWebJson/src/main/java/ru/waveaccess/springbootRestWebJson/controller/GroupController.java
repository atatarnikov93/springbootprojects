package ru.waveaccess.springbootRestWebJson.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.*;
import ru.waveaccess.springbootRestWebJson.domain.Group;
import ru.waveaccess.springbootRestWebJson.services.GroupService;

import javax.validation.constraints.NotBlank;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.Optional;

@RestController
@RequestMapping("rest/group")
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @GetMapping
    public Iterable<Group> getList() {
        return groupService.findAll();
    }

    @DeleteMapping("{id}")
    public void deleteGroup(@PathVariable Long id) {
        Optional<Group> foundGroup = groupService.findGroup(id);
        foundGroup.ifPresent(group -> groupService.deleteGroup(group));
    }

    @PostMapping
    public void createGroup(@RequestBody CreateGroupRequest createGroupRequest) {
        Group newGroup = new Group();
        newGroup.setGroupName(createGroupRequest.getGroupName());
        newGroup.setCreated(ZonedDateTime.now(Clock.systemUTC()));
        groupService.createGroup(newGroup);
    }

    @PutMapping({"id"})
    public void updateGroup(@RequestBody CreateGroupRequest createGroupRequest) {
        Long id = createGroupRequest.getId();
        Optional<Group> foundGroup = groupService.findGroup(id);
        foundGroup.ifPresent(newGroup -> {
            newGroup.setGroupName(createGroupRequest.getGroupName());
            newGroup.setUpdated(ZonedDateTime.now(Clock.systemUTC()));
            groupService.updateGroup(newGroup);
        });
    }

    @Getter
    @Setter
    private static class CreateGroupRequest{
        @NotBlank
        private Long id;

        @NotBlank
        private String groupName;

        private ZonedDateTime created;;
        private ZonedDateTime updated;;
    }
}


