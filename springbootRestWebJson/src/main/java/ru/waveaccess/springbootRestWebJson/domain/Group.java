package ru.waveaccess.springbootRestWebJson.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Clock;
import java.time.ZonedDateTime;

@Entity
@Data
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String groupName;

    private ZonedDateTime created;
    private ZonedDateTime updated;

    public Group() {
    }

    public Group(@NotBlank String groupName) {
        this.groupName = groupName;
        this.created = ZonedDateTime.now(Clock.systemUTC());
    }
}
