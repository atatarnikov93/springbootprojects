package ru.waveaccess.springbootRestWebJson.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min=2, message = "First name should have atleast 2 characters")
    @NotBlank
    private String firstName;

    private String middleName;

    @Size(min=2, message = "Last name should have atleast 2 characters")
    @NotBlank
    private String lastName;

    @NotNull
    @Column
    private LocalDate birthday;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    @NotNull
    private Group group;
}
