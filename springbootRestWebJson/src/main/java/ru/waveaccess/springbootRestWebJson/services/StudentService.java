package ru.waveaccess.springbootRestWebJson.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.waveaccess.springbootRestWebJson.domain.Group;
import ru.waveaccess.springbootRestWebJson.domain.Student;
import ru.waveaccess.springbootRestWebJson.repositories.GroupRepository;
import ru.waveaccess.springbootRestWebJson.repositories.StudentRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

    public Iterable<Student> findAll() {
        return studentRepository.findAll();
    }

    public void createStudent(Student student) {
        try {
            Group group = groupRepository.findById(student.getGroup().getId()).get();
            student.setGroup(group);
            studentRepository.save(student);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateStudent(Student student) {
        Group group = groupRepository.findById(student.getGroup().getId()).get();
        student.setGroup(group);
        studentRepository.save(student);
    }

    public Optional<Student> findStudent(Long id) {
        return studentRepository.findById(id);
    }

    public void deleteStudent(Student student) {
        studentRepository.deleteById(student.getId());
    }


}
