package ru.waveaccess.springbootRestWebJson.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.waveaccess.springbootRestWebJson.domain.Group;
import ru.waveaccess.springbootRestWebJson.repositories.GroupRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository groupRepository;

    public void createGroup(Group group){
        groupRepository.save(group);
    }

    public void updateGroup(Group group){
        groupRepository.save(group);
    }

    public Optional<Group> findGroup(Long id) {
        return groupRepository.findById(id);
    }

    public void deleteGroup(Group group){
        groupRepository.deleteById(group.getId());
    }

    public Iterable<Group> findAll(){
        return groupRepository.findAll();
    }

}
